//
// TemplatingHost.cs
//
// Author:
//       Mikayla Hutchinson <m.j.hutchinson@gmail.com>
//
// Copyright (c) 2009 Novell, Inc. (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using TextTemplating.Microsoft.VisualStudio.TextTemplating;

namespace TextTemplating
{
    public class TemplateGenerator : MarshalByRefObject, ITextTemplatingEngineHost
    {
        private static readonly Encoding BomlessUtf8Encoding = new UTF8Encoding(false);

        private readonly Dictionary<string, KeyValuePair<string, string>> _directiveProcessors =
            new Dictionary<string, KeyValuePair<string, string>>();

        //host fields

        private readonly Dictionary<ParameterKey, string> _parameters = new Dictionary<ParameterKey, string>();

        private Encoding _encoding;

        //re-usable
        private TemplatingEngine _engine;

        //per-run variables
        private string _inputFile;

        public TemplateGenerator()
        {
            Refs.Add(typeof(TextTransformation).Assembly.Location);
            Refs.Add(typeof(Uri).Assembly.Location);
            Imports.Add("System");
        }

        //host properties for consumers to access
        public CompilerErrorCollection Errors { get; } = new CompilerErrorCollection();
        public List<string> Refs { get; } = new List<string>();
        public List<string> Imports { get; } = new List<string>();
        public List<string> IncludePaths { get; } = new List<string>();
        public List<string> ReferencePaths { get; } = new List<string>();
        private string OutputFile { get; set; }
        private bool UseRelativeLinePragmas { get; set; }

        private TemplatingEngine Engine => _engine ?? (_engine = new TemplatingEngine());

        /// <summary>
        ///     If non-null, the template's Host property will be the full type of this host.
        /// </summary>
        public virtual Type SpecificHostType => null;

        public CompiledTemplate CompileTemplate(string content)
        {
            if (string.IsNullOrEmpty(content))
                throw new ArgumentNullException("content");

            Errors.Clear();
            _encoding = Encoding.UTF8;

            return Engine.CompileTemplate(content, this);
        }

        public bool ProcessTemplate(string inputFile, string outputFile)
        {
            if (string.IsNullOrEmpty(inputFile))
                throw new ArgumentNullException("inputFile");
            if (string.IsNullOrEmpty(outputFile))
                throw new ArgumentNullException("outputFile");

            string content;
            try
            {
                content = File.ReadAllText(inputFile);
            }
            catch (IOException ex)
            {
                Errors.Clear();
                AddError("Could not read input file '" + inputFile + "':\n" + ex);
                return false;
            }

            string output;
            ProcessTemplate(inputFile, content, ref outputFile, out output);

            try
            {
                if (!Errors.HasErrors)
                    WriteAllTextToFile(outputFile, output, _encoding);
            }
            catch (IOException ex)
            {
                AddError("Could not write output file '" + outputFile + "':\n" + ex);
            }

            return !Errors.HasErrors;
        }

        public bool ProcessTemplate(string inputFileName, string inputContent, ref string outputFileName,
            out string outputContent)
        {
            Errors.Clear();
            _encoding = Encoding.UTF8;

            OutputFile = outputFileName;
            _inputFile = inputFileName;
            outputContent = Engine.ProcessTemplate(inputContent, this);
            outputFileName = OutputFile;

            return !Errors.HasErrors;
        }

        public bool PreprocessTemplate(string tmpInputFile, string className, string classNamespace,
            string tmpOutputFile, Encoding tmpEncoding, out string language, out string[] references)
        {
            language = null;
            references = null;

            if (string.IsNullOrEmpty(tmpInputFile))
                throw new ArgumentNullException(nameof(tmpInputFile));
            if (string.IsNullOrEmpty(tmpOutputFile))
                throw new ArgumentNullException(nameof(tmpOutputFile));

            string content;
            try
            {
                content = File.ReadAllText(tmpInputFile);
            }
            catch (IOException ex)
            {
                Errors.Clear();
                AddError("Could not read input file '" + tmpInputFile + "':\n" + ex);
                return false;
            }

            PreprocessTemplate(tmpInputFile, className, classNamespace, content, out language, out references, out var output);

            try
            {
                if (!Errors.HasErrors)
                    WriteAllTextToFile(tmpOutputFile, output, tmpEncoding);
            }
            catch (IOException ex)
            {
                AddError("Could not write output file '" + tmpOutputFile + "':\n" + ex);
            }

            return !Errors.HasErrors;
        }

        private static void WriteAllTextToFile(string path, string contents, Encoding encoding)
        {
            File.WriteAllText(path, contents,
                (encoding is UTF8Encoding || "utf-8".Equals(encoding.WebName, StringComparison.OrdinalIgnoreCase))
                && encoding != BomlessUtf8Encoding
                    ? BomlessUtf8Encoding
                    : encoding);
        }

        public bool PreprocessTemplate(string inputFileName, string className, string classNamespace,
            string inputContent,
            out string language, out string[] references, out string outputContent)
        {
            Errors.Clear();
            _encoding = Encoding.UTF8;

            _inputFile = inputFileName;
            outputContent = Engine.PreprocessTemplate(inputContent, this, className, classNamespace, out language,
                out references);

            return !Errors.HasErrors;
        }

        private CompilerError AddError(string error)
        {
            var err = new CompilerError {ErrorText = error};
            Errors.Add(err);
            return err;
        }

        public void AddDirectiveProcessor(string name, string klass, string assembly)
        {
            _directiveProcessors.Add(name, new KeyValuePair<string, string>(klass, assembly));
        }

        public void AddParameter(string processorName, string directiveName, string parameterName, string value)
        {
            _parameters.Add(new ParameterKey(processorName, directiveName, parameterName), value);
        }

        /// <summary>
        ///     Parses a parameter and adds it.
        /// </summary>
        /// <returns>Whether the parameter was parsed successfully.</returns>
        /// <param name="unparsedParameter">Parameter in name=value or processor!directive!name!value format.</param>
        public bool TryAddParameter(string unparsedParameter)
        {
            string processor, directive, name, value;
            if (TryParseParameter(unparsedParameter, out processor, out directive, out name, out value))
            {
                AddParameter(processor, directive, name, value);
                return true;
            }

            return false;
        }

        public static bool TryParseParameter(string parameter, out string processor, out string directive,
            out string name, out string value)
        {
            processor = directive = name = value = "";

            var start = 0;
            var end = parameter.IndexOfAny(new[] {'=', '!'});
            if (end < 0)
                return false;

            //simple format n=v
            if (parameter[end] == '=')
            {
                name = parameter.Substring(start, end);
                value = parameter.Substring(end + 1);
                return !string.IsNullOrEmpty(name);
            }

            //official format, p!d!n!v
            processor = parameter.Substring(start, end);

            start = end + 1;
            end = parameter.IndexOf('!', start);
            if (end < 0)
            {
                //unlike official version, we allow you to omit processor/directive
                name = processor;
                value = parameter.Substring(start);
                processor = "";
                return !string.IsNullOrEmpty(name);
            }

            directive = parameter.Substring(start, end - start);


            start = end + 1;
            end = parameter.IndexOf('!', start);
            if (end < 0)
            {
                //we also allow you just omit the processor
                name = directive;
                directive = processor;
                value = parameter.Substring(start);
                processor = "";
                return !string.IsNullOrEmpty(name);
            }

            name = parameter.Substring(start, end - start);
            value = parameter.Substring(end + 1);

            return !string.IsNullOrEmpty(name);
        }

        protected virtual bool LoadIncludeText(string requestFileName, out string content, out string location)
        {
            content = "";
            location = ResolvePath(requestFileName);

            if (location == null || !File.Exists(location))
                foreach (var path in IncludePaths)
                {
                    var f = Path.Combine(path, requestFileName);
                    if (File.Exists(f))
                    {
                        location = f;
                        break;
                    }
                }

            if (location == null)
                return false;

            try
            {
                content = File.ReadAllText(location);
                return true;
            }
            catch (IOException ex)
            {
                AddError("Could not read included file '" + location + "':\n" + ex);
            }

            return false;
        }

        /// <summary>
        ///     Gets any additional directive processors to be included in the processing run.
        /// </summary>
        public virtual IEnumerable<IDirectiveProcessor> GetAdditionalDirectiveProcessors()
        {
            yield break;
        }

        private struct ParameterKey : IEquatable<ParameterKey>
        {
            public ParameterKey(string processorName, string directiveName, string parameterName)
            {
                _processorName = processorName ?? "";
                _directiveName = directiveName ?? "";
                _parameterName = parameterName ?? "";
                unchecked
                {
                    _hashCode = _processorName.GetHashCode()
                               ^ _directiveName.GetHashCode()
                               ^ _parameterName.GetHashCode();
                }
            }

            private readonly string _processorName;
            private readonly string _directiveName;
            private readonly string _parameterName;
            private readonly int _hashCode;

            public override bool Equals(object obj)
            {
                return obj is ParameterKey && Equals((ParameterKey) obj);
            }

            public bool Equals(ParameterKey other)
            {
                return _processorName == other._processorName && _directiveName == other._directiveName &&
                       _parameterName == other._parameterName;
            }

            public override int GetHashCode()
            {
                return _hashCode;
            }
        }

        #region Virtual members

        public virtual object GetHostOption(string optionName)
        {
            if (optionName == "UseRelativeLinePragmas") return UseRelativeLinePragmas;

            return null;
        }

        public virtual AppDomain ProvideTemplatingAppDomain(string content)
        {
            return null;
        }

        protected virtual string ResolveAssemblyReference(string assemblyReference)
        {
            if (Path.IsPathRooted(assemblyReference))
                return assemblyReference;
            foreach (var referencePath in ReferencePaths)
            {
                var path = Path.Combine(referencePath, assemblyReference);
                if (File.Exists(path))
                    return path;
            }

            var assemblyName = new AssemblyName(assemblyReference);
            if (assemblyName.Version != null) //Load via GAC and return full path
                return Assembly.Load(assemblyName).Location;

            if (!assemblyReference.EndsWith(".dll", StringComparison.OrdinalIgnoreCase) &&
                !assemblyReference.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
                return assemblyReference + ".dll";
            return assemblyReference;
        }

        protected virtual string ResolveParameterValue(string directiveId, string processorName, string parameterName)
        {
            var key = new ParameterKey(processorName, directiveId, parameterName);
            string value;
            if (_parameters.TryGetValue(key, out value))
                return value;
            if (processorName != null || directiveId != null)
                return ResolveParameterValue(null, null, parameterName);
            return null;
        }

        protected virtual Type ResolveDirectiveProcessor(string processorName)
        {
            if (!_directiveProcessors.TryGetValue(processorName, out var value))
                throw new Exception($"No directive processor registered as '{processorName}'");
            var asmPath = ResolveAssemblyReference(value.Value);
            if (asmPath == null)
                throw new Exception(
                    $"Could not resolve assembly '{value.Value}' for directive processor '{processorName}'");
            var asm = Assembly.LoadFrom(asmPath);
            return asm.GetType(value.Key, true);
        }

        protected virtual string ResolvePath(string path)
        {
            path = Environment.ExpandEnvironmentVariables(path);
            if (Path.IsPathRooted(path))
                return path;
            var dir = Path.GetDirectoryName(_inputFile);
            var test = Path.Combine(dir ?? throw new DirectoryNotFoundException(_inputFile), path);
            if (File.Exists(test) || Directory.Exists(test))
                return test;
            return path;
        }

        #endregion

        #region Explicit ITextTemplatingEngineHost implementation

        bool ITextTemplatingEngineHost.LoadIncludeText(string requestFileName, out string content, out string location)
        {
            return LoadIncludeText(requestFileName, out content, out location);
        }

        void ITextTemplatingEngineHost.LogErrors(CompilerErrorCollection errors)
        {
            Errors.AddRange(errors);
        }

        string ITextTemplatingEngineHost.ResolveAssemblyReference(string assemblyReference)
        {
            return ResolveAssemblyReference(assemblyReference);
        }

        string ITextTemplatingEngineHost.ResolveParameterValue(string directiveId, string processorName,
            string parameterName)
        {
            return ResolveParameterValue(directiveId, processorName, parameterName);
        }

        Type ITextTemplatingEngineHost.ResolveDirectiveProcessor(string processorName)
        {
            return ResolveDirectiveProcessor(processorName);
        }

        string ITextTemplatingEngineHost.ResolvePath(string path)
        {
            return ResolvePath(path);
        }

        void ITextTemplatingEngineHost.SetFileExtension(string extension)
        {
            extension = extension.TrimStart('.');
            if (Path.HasExtension(OutputFile))
                OutputFile = Path.ChangeExtension(OutputFile, extension);
            else
                OutputFile = OutputFile + "." + extension;
        }

        void ITextTemplatingEngineHost.SetOutputEncoding(Encoding encoding, bool fromOutputDirective)
        {
            _encoding = encoding;
        }

        IEnumerable<string> ITextTemplatingEngineHost.StandardAssemblyReferences => Refs;

        IEnumerable<string> ITextTemplatingEngineHost.StandardImports => Imports;

        string ITextTemplatingEngineHost.TemplateFile => _inputFile;

        #endregion
    }
}