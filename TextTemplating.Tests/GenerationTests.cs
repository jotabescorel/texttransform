//
// GenerationTests.cs
//
// Author:
//       Mikayla Hutchinson <m.j.hutchinson@gmail.com>
//
// Copyright (c) 2009 Novell, Inc. (http://www.novell.com)
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;
using TextTemplating.Microsoft.VisualStudio.TextTemplating;

namespace TextTemplating.Tests
{
    [TestFixture]
    public class GenerationTests
    {
        //NOTE: we set the newline property on the code generator so that the whole files has matching newlines,
        // in order to match the newlines in the verbatim code blocks
        private void Generate(string input, string expectedOutput, string newline)
        {
            var host = new DummyHost();
            var className = "GeneratedTextTransformation4f504ca0";
            var code = GenerateCode(host, input, className, newline);
            Assert.AreEqual(0, host.Errors.Count);

            var generated = TemplatingEngineHelper.CleanCodeDom(code, newline);
            expectedOutput = TemplatingEngineHelper.CleanCodeDom(expectedOutput, newline);
            Assert.AreEqual(expectedOutput, generated);
        }

        private string GenerateCode(ITextTemplatingEngineHost host, string content, string name,
            string generatorNewline)
        {
            var pt = ParsedTemplate.FromText(content, host);
            if (pt.Errors.HasErrors)
            {
                host.LogErrors(pt.Errors);
                return null;
            }

            var settings = TemplatingEngine.GetSettings(host, pt);
            if (name != null)
                settings.Name = name;
            if (pt.Errors.HasErrors)
            {
                host.LogErrors(pt.Errors);
                return null;
            }

            var ccu = TemplatingEngine.GenerateCompileUnit(host, content, pt, settings);
            if (pt.Errors.HasErrors)
            {
                host.LogErrors(pt.Errors);
                return null;
            }

            var opts = new CodeGeneratorOptions();
            using (var writer = new StringWriter())
            {
                writer.NewLine = generatorNewline;
                settings.Provider.GenerateCodeFromCompileUnit(ccu, writer, opts);
                return writer.ToString();
            }
        }

        public static string OutputSample1 =
            @"
namespace TextTemplating.Microsoft.VisualStudio.TextTemplating {
    
    
    public partial class GeneratedTextTransformation4f504ca0 : global::TextTemplating.Microsoft.VisualStudio.TextTemplating.TextTransformation {
        
        
        #line 9 """"

baz \#>

        #line default
        #line hidden
        
        public override string TransformText() {
            this.GenerationEnvironment = null;
            
            #line 2 """"
            this.Write(""Line One\nLine Two\n"");
            
            #line default
            #line hidden
            
            #line 4 """"

foo

            
            #line default
            #line hidden
            
            #line 7 """"
            this.Write(""Line Three "");
            
            #line default
            #line hidden
            
            #line 7 """"
            this.Write(global::TextTemplating.Microsoft.VisualStudio.TextTemplating.ToStringHelper.ToStringWithCulture( bar ));
            
            #line default
            #line hidden
            
            #line 7 """"
            this.Write(""\nLine Four\n"");
            
            #line default
            #line hidden
            return this.GenerationEnvironment.ToString();
        }
        
        public override void Initialize() {
            base.Initialize();
        }
    }
}
";

        [Test]
        public void DefaultLanguage()
        {
            var host = new DummyHost();
            var template = @"<#= DateTime.Now #>";
            var pt = ParsedTemplate.FromText(template, host);
            Assert.AreEqual(0, host.Errors.Count);
            var settings = TemplatingEngine.GetSettings(host, pt);
            Assert.AreEqual(settings.Language, "C#");
        }

        [Test]
        public void Generate()
        {
            var Input = ParsingTests.ParseSample1.Replace(Environment.NewLine, "\n");
            ;
            var Output = OutputSample1;
            Generate(Input, Output, "\n");
        }

        [Test]
        public void GenerateMacNewlines()
        {
            var MacInput = ParsingTests.ParseSample1.Replace(Environment.NewLine, "\r");
            var MacOutput = OutputSample1.Replace("\\n", "\\r").Replace(Environment.NewLine, "\r");
            ;
            Generate(MacInput, MacOutput, "\r");
        }

        [Test]
        public void GenerateWindowsNewlines()
        {
            var WinInput = ParsingTests.ParseSample1.Replace(Environment.NewLine, "\r\n");
            var WinOutput = OutputSample1.Replace("\\n", "\\r\\n").Replace(Environment.NewLine, "\r\n");
            Generate(WinInput, WinOutput, "\r\n");
        }

        [Test]
        public void ImportLocalDirectoryReferencesTest()
        {
            var gen = new TemplateGenerator();
            string tmp = null;
            gen.ReferencePaths.Add(Path.GetDirectoryName(typeof(Uri).Assembly.Location));
            gen.ReferencePaths.Add(Path.GetDirectoryName(typeof(Enumerable).Assembly.Location));
            gen.ReferencePaths.Add(Path.GetDirectoryName(typeof(JsonConvert).Assembly.Location));

            gen.ProcessTemplate(null, @"
                <#@ assembly name=""System.dll"" #>
                <#@ assembly name=""System.Core.dll"" #>
                <#@ assembly name=""Newtonsoft.Json.dll"" #>
                <#@ import namespace=""Newtonsoft.Json"" #>
                <#
                    var content = JsonConvert.SerializeObject(""hello"");
                #>
                ", ref tmp, out tmp);

            Assert.AreEqual(0, gen.Errors.Count, nameof(ImportLocalDirectoryReferencesTest));
        }

        [Test]
        public void ImportReferencesTest()
        {
            var gen = new TemplateGenerator();
            string tmp = null;
            gen.ReferencePaths.Add(Path.GetDirectoryName(typeof(Uri).Assembly.Location));
            gen.ReferencePaths.Add(Path.GetDirectoryName(typeof(Enumerable).Assembly.Location));
            gen.ProcessTemplate(null, "<#@ assembly name=\"System.dll\" #>\n<#@ assembly name=\"System.Core.dll\" #>",
                ref tmp, out tmp);
            Assert.AreEqual(0, gen.Errors.Count, "ImportReferencesTest");
        }

        [Test]
        public void TemplateGeneratorTest()
        {
            var gen = new TemplateGenerator();
            string tmp = null;
            gen.ProcessTemplate(null, "<#@ template language=\"C#\" #>", ref tmp, out tmp);
            Assert.AreEqual(0, gen.Errors.Count, "ProcessTemplate");
        }
    }
}